
NAME=stonks
UUID=stonks@0x41ndrea.gitlab.com
EXTENSIONS_DIR=$(HOME)/.local/share/gnome-shell/extensions

clean:
	rm -rf $(EXTENSIONS_DIR)/$(UUID)

schema:
	glib-compile-schemas $(UUID)/schemas/

install: clean schema
	mkdir -p $(EXTENSIONS_DIR)
	cp -r $(UUID) $(EXTENSIONS_DIR)/

test-session: install
	dbus-run-session -- gnome-shell --nested --wayland

$(NAME).zip: schema
	cd $(UUID) && zip -r ../$(NAME).zip ./*

.PHONY: clean schema install $(NAME).zip test-session
