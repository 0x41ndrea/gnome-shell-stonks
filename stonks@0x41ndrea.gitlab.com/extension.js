/* extension.js
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-2.0-or-later
 */

/* exported init */
const Clutter = imports.gi.Clutter;
const ExtensionUtils = imports.misc.extensionUtils;

const Gettext = imports.gettext;
const _ = Gettext.domain('stonks').gettext;

const Main = imports.ui.main;
const Me = imports.misc.extensionUtils.getCurrentExtension();
const PanelMenu = imports.ui.panelMenu;
const PopupMenu = imports.ui.popupMenu;
const Soup = imports.gi.Soup;
const Util = imports.misc.util;

const {GObject, St, Gio} = imports.gi;


class YahooStockInfoProvider {

    constructor() {
        this.httpSession = new Soup.Session();
    }

    static get INFO_URL() {
        return 'https://finance.yahoo.com/quote/';
    }

    static get URL() {
        return 'https://query1.finance.yahoo.com/v6/finance/quote';
    }

    get_price(name, cb, on_err) {
        if (name === null || name === undefined)
            return on_err('unable to load price for an empty stock ticker');

        let message = Soup.Message.new('GET', `${YahooStockInfoProvider.URL}?lang=en-Us&corsDomain=inance.yahoo.com&symbols=${name}`);

        this.httpSession.send_and_read_async(message, Gio.PRIORITY_HIGH, null, (session, result) => {
            let response;
            if (message.get_status() === Soup.Status.OK) {
                let bytes = session.send_and_read_finish(result);
                // eslint-disable-next-line no-undef
                let decoder = new TextDecoder('utf-8');
                response = decoder.decode(bytes.get_data());
                let stock_info = JSON.parse(response);
                stock_info.quoteResponse.result.forEach(si => {
                    cb(si.regularMarketPrice,
                        si.regularMarketChange,
                        si.regularMarketChangePercent);
                });
            } else {
                on_err(response);
            }
        },
        );
    }
}

// keep track of UI objects individually for easier lookup
class StockItem {
    constructor(name, item, price, change) {
        this.symbol = name;
        this.item = item;
        this.price = price;
        this.change = change;
    }

    destroy() {
        this.item.destroy();
        this.item = null;
    }
}


const Stonks = GObject.registerClass(
    class Stonks extends PanelMenu.Button {
        _init() {
            super._init(1, Gettext.gettext('Stonks'));
            this._settings = ExtensionUtils.getSettings('org.gnome.shell.extensions.stonks');
            this.__index = new Map();
            this.show_changes_p = false;

            let icon = new St.Icon({
                style_class: 'system-status-icon',
            });
            icon.set_gicon(Gio.icon_new_for_string(`${Me.path}/icons/chart.svg`));
            this.add_child(icon);

            this.menu.addMenuItem(this.searchSection());
            this.menu.addMenuItem(new PopupMenu.PopupSeparatorMenuItem());
            this.menu.connect('open-state-changed', (actor, open) => {
                if (open)
                    this.refresh_prices();
            });

            this.stock_provider = new YahooStockInfoProvider();
            this.load_symbols();
            this._debug = this._settings.get_boolean('debug');
        }

        _log(message) {
            if (this._debug === true)
                log(`STONKS: ${message}`);
        }

        load_symbols() {

            let symbols = this._settings.get_strv('stocks');
            this._log(`Loaded ${symbols}`);
            symbols.forEach(symbol => {
                this.new_item(symbol);
            });
        }

        searchSection() {
            let searchSection = new PopupMenu.PopupMenuSection();

            let search = new St.Entry({
                name: 'newCompanyEntry',
                hint_text: Gettext.gettext('Stock Symbol'),
                track_hover: true,
                can_focus: true,
            });

            let query = search.clutter_text;

            query.connect('key_press_event', (actor, event) => {
                if (event.get_key_symbol() === Clutter.KEY_Return) {
                    this.menu.toggle();
                    let tickers = actor.get_text().toUpperCase();
                    // allow list of tickers in search box
                    tickers = tickers.split(',');
                    tickers.forEach(t => this.new_item(t.trim()));
                    search.set_text('');
                }
            });

            searchSection.actor.add_actor(search);
            searchSection.actor.add_style_class_name('newCompanySection');
            return searchSection;
        }

        refresh_stock(stock) {
            if (this.__index.get(stock.symbol) === undefined) {
                this.menu.addMenuItem(stock.item);
                // keep track of the item for deletion
                this.__index.set(stock.symbol, stock);
                this.save_stocks();
            }
            this.stock_provider.get_price(stock.symbol, (mkt_price, mkt_chng, mkt_chng_p) => {
                stock.price.set_text(String(mkt_price.toFixed(2)));
                let chng = mkt_chng;
                let label = '';
                let pc = '';
                if (this.show_changes_p) {
                    chng = mkt_chng_p;
                    pc = '%';
                }
                chng = chng.toFixed(3);

                label = `+${chng}${pc}`;
                if (chng < 0) {
                    label = `-${Math.abs(chng)}${pc}`;
                    stock.change.set_style('background-color: #f94848');
                } else {
                    stock.change.set_style('background-color: #67db3b');
                }
                stock.change.set_label(label);
            }, message => {
                // grey out change box on connection errors
                this._log(`Got ${message.status_code} from stock price provider, disabling`);
                for (const item of this.__index.values())
                    item.change.set_style('background-color: grey');
            });
        }

        refresh_prices() {
            for (const [symbol, item] of this.__index.entries()) {
                this._log(`refresh prices for ${symbol}`);
                this.refresh_stock(item);
            }
        }

        save_stocks() {
            let keys = Array.from(this.__index.keys());
            this._settings.set_strv('stocks', keys);
            this._log(`saved stocks in gsettings ${keys}`);
        }

        // toggles price change from dollars to pc
        // TODO: add market cap on third click
        toggle_price_changes() {
            this.show_changes_p = !this.show_changes_p;
            this.refresh_prices();
        }

        new_item(name) {
            let button = new St.BoxLayout(
                {
                    style_class: 'StBoxLayout',
                    reactive: true,
                    can_focus: true,
                    track_hover: true,
                });

            // stock ticker symbol
            let symbol = new St.Label({style_class: 'symbol-label', text: name});

            // current price on open market
            let price = new St.Label({style_class: 'status-label', text: 'NA'});

            // ditto
            let spacer = new St.Label({style_class: 'spacer', text: ''});

            // price change in dollars
            let change = new St.Button({label: ' NA ', style_class: 'price-change'});

            // eslint-disable-next-line no-shadow
            change.connect('button_press_event', _ => {
                this.toggle_price_changes();
            });

            button.insert_child_at_index(symbol, 1);
            button.insert_child_at_index(spacer, 2);
            button.insert_child_at_index(price, 3);
            button.insert_child_at_index(change, 4);

            // eslint-disable-next-line no-shadow
            button.connect('button_press_event', _ => {
                Util.spawn(['xdg-open',  YahooStockInfoProvider.INFO_URL + name]);
            });

            let item = new PopupMenu.PopupMenuItem('');

            item.add_actor(button);

            let remove_icon = new St.Icon(
                {
                    icon_name: 'edit-delete',
                    reactive: true,
                    can_focus: true,
                    style_class: 'popup-menu-icon',
                    track_hover: true,
                },
            );

            remove_icon.connect('button_press_event', actor => {
                this._log(`Destroying actor ${actor.__tag}`);

                if (this.__index.get(actor.__tag) !== undefined) {
                    this.__index.get(actor.__tag).destroy();
                    this.__index.delete(actor.__tag);
                } else {
                    this._log(`unable to delete ${actor.__tag} from index`);
                }
                this.save_stocks();
            });

            // tag the icon and item for indexes lookups
            remove_icon.__tag = name;
            item.add_actor(remove_icon);

            let stock = new StockItem(name, item, price, change);

            this.refresh_stock(stock);
        }
    });

function init() {
    ExtensionUtils.initTranslations('stonks');
}

let stonks;
// eslint-disable-next-line no-unused-vars
function enable() {
    stonks = new Stonks();
    Main.panel.addToStatusArea('stonks', stonks, 1);
}

// eslint-disable-next-line no-unused-vars
function disable() {
    stonks.destroy();
    stonks = null;
}
